import argparse
import json

from jinja2 import Environment, FileSystemLoader


def main(args):
    environment = Environment(loader=FileSystemLoader("templates/"))
    template = environment.get_template(f"{args.template}.svg.j2")
    with open(f"colors/{args.colors}.json") as colors:
        print(template.render(json.load(colors)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Render templates into images",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-c",
        "--colors",
        default="original",
        help="The colors file to load. The colors file should contain the variables required for the template.",
    )
    parser.add_argument(
        "-t",
        "--template",
        default="original",
        help="The template to load from the templates directory.",
    )

    main(parser.parse_args())
