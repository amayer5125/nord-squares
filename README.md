# Nord Squares

A set of wallpapers made to work with [Nord Theme](https://www.nordtheme.com/).

This project is based on a wallpaper I saw on [r/unixporn](https://www.reddit.com/r/unixporn/comments/h163ns/i3_gruvy_x1/fukth5j/).

## Templates

Each template has a different design. Templates are written using the Jinja2 templating language.

Most templates have a background and default color as well as five or six accent colors.

## Colors

Color files are json files containing a flat dictionary of variables to set while rendering a template.

## Building

To create an SVG file, choose a template and a color scheme then run the render python file.

```sh
# original colors and template (defaults)
python render.py > original.svg

# choose a different color scheme or template
python render.py --colors rainbow --template stripe-diagonal-six > rainbow-diagonal.svg
```

If you would like a PNG or JPG file instead you can use the `convert` command provided by ImageMagick.

```sh
python render.py --colors rainbow | convert - rainbow.png
```

You can also use the `convert` command to resize the final image.

```sh
python render.py --colors cool | convert - -resize 1920x1080 cool.png
```
